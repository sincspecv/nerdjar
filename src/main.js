// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
// import Welcome from './components/Welcome.vue'
import router from './router'

import store from './vuex/store'

Vue.config.productionTip = false

//Filters
Vue.filter('currency', function (value) {
  return '$' + parseFloat(value).toFixed(2);
})



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  data: {
    ds: null,
  },
  created: function() {
    this.ds = deepstream('wss://013.deepstreamhub.com?apiKey=a7709b94-3d12-484d-ac22-2d07baeb259c')
      .login()
  },
  template: '<App/>',
  components: { App }
});
