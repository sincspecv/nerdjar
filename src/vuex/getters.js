export default {
  getTxs(state) {
    return state.user.transactions
  },
  getPendingContributions(state) {
    let pendingContributions = 0.0;
    for(let i = 0; i < state.user.transactions.length; i++) {
      if(state.user.transactions[i].txStatus === 'pending') {
        pendingContributions += state.user.transactions[i].txAmount
      }
    }
    return pendingContributions
  },
  getGroupTotal(state) {
    let groupContributions = 0.0;
    for(let i = 0; i < state.group.transactions.length; i++) {
      if(state.group.transactions[i].txStatus === 'pending') {
        groupContributions += state.group.transactions[i].txAmount
      }
    }
    return groupContributions
  },
  getUserName(state) {
    return state.user.name
  },
  getRealName(state) {
    const realName = `${state.user.realFirstName} ${state.user.realLastName}`
    return realName
  },
  getIcons(state) {
    return state.icons
  }
}