const ADD_TRANSACTION = 'ADD_TRANSACTION'
const CANCEL_TRANSACTION = 'CANCEL_TRANSACTION'

export default {
  [ADD_TRANSACTION](state, payload) {
    const user = state.user.name
    const group = state.group.name
    let newTx = {
      txId: state.group.transactions.length,
      txAmount: payload.amount,
      txDate: new Date().toISOString().slice(0, 10),
      txStatus: 'pending',
      user: user,
      group: group
    }
    state.user.transactions.push(newTx)
    state.group.transactions.push(newTx)
  },
  [CANCEL_TRANSACTION](state, payload) {

    console.log(payload)

    if(payload > -1) {
      const userTxIndex = _.findIndex(state.user.transactions, function(o){return o.txId == payload})
      state.user.transactions[userTxIndex].txStatus = 'cancelled'

      const groupTxIndex = _.findIndex(state.group.transactions, function(o){return o.txId == payload})
      state.group.transactions[groupTxIndex].txStatus = 'cancelled'
    }
  }
}