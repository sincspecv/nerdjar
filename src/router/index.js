import Vue from 'vue'
import Router from 'vue-router'
import Hello from './routes/Hello'
import Menu from './routes/Menu'
// import Welcome from '@/components/Welcome'
import Add from './routes/Add'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'menu',
      components: {
        default: Menu,
        add: Add
      }
    },
    {
      path: '/add',
      name: 'Add',
      component: Add
    }
  ]
})
